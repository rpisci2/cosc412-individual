from django.urls import path
from . import views
from web_files.views import views

urlpatterns = [
    path('(?P<page_name>[^/]+)/edit/$', views.edit_page),
    path('(?P<page_name>[^/]+)/save/$', views.save_page),
    path('?P<page_name>[^/]+)/$', views.view_page)
]