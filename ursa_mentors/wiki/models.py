from django.db import models

# Create your models here.

class Page(models.Model): #Wiki is just a collection of pages and we will inherit from Django Model Class
    name = models.CharField(max_length=20, primary_key=True) #When Django creates tables it will set primary key to true
    content = models.TextField(blank=True) #content can be blank 