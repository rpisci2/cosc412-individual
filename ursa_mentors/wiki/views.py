from django.shortcuts import render, redirect
from . import views
from web_files import views
from . import models
from wiki.models import Page
from django.http import HttpResponseRedirect

# Create your views here.
def view_page(request, page_name):
    try:
        page = Page.objects.get(pk=page_name)
    except Page.DoesNotExist:
        return render("create.html", {"page_name":page_name})
    content = page.content
    return render("view.html", {"page_name":page_name, "content":content})

def edit_page(request, page_name):
    try:
        page = Page.objects.get(pk=page_name)
        content = page.content
    except Page.DoesNotExist:
        content = ""
    return render("edit.html", {"page_name":page_name, "content":content})

def save_page(request, page_name):
    content = request.POST["content"]
    try:
        page = Page.objects.get(pk=page_name)
        page.content = content
    except Page.DoesNotExist:
        page = Page(name=page_name, content=content)
        page.save()
    return HttpResponseRedirect(page_name + "/")