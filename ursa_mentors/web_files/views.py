from django.shortcuts import render
from wiki import views

# Create your views here.

def index(request):
    return render(request, 'web_files/index.html')

def view_page(request):
    return render(request, 'wiki/view.html')

def edit_page(request):
    return render(request, 'wiki/edit.html')

def save_page(request):
    return render(request, 'wiki/create.html')