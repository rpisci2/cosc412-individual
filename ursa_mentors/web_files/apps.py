from django.apps import AppConfig


class WebFilesConfig(AppConfig):
    name = 'web_files'
