from django.urls import path
from . import views
from wiki.views import views

urlpatterns = [
    path('', views.index),
    path('edit_page', views.edit_page),
    path('save_page', views.save_page),
    path('view_page', views.view_page)
]