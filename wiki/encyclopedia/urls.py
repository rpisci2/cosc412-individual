from django.urls import path
from django.urls import include, path

from . import views

urlpatterns = [
    path("", views.index, name="index"),
    path("wiki/<str:entry>", views.entry, name="entry"),
    path("newEntry", views.newEntry, name="newEntry"),
    path("wiki/<str:entry>/edit", views.edit, name="edit"),
    path("random", views.random, name="random"),
    path("search", views.search, name="search"),
    path('landing/',views.landing, name="landing"),
    path('warning', views.warning, name="warning"),
    path('', include("accounts.urls")) #Import urls from accounts app 
]
