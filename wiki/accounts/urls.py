from django.urls import path
from . import views 
from django.contrib.auth.views import LoginView, LogoutView
from django.conf.urls.static import static

from django.urls import include, path

urlpatterns = [
    path('', views.index, name="index"),
    path('dashboard/', views.dashboard, name="dashboard"),
    path('login/', LoginView.as_view(), name="login"),
    path('register/', views.register, name="register"),
    path('captcha_fail/', views.captcha_fail, name="captcha_fail"),
    path('logout/', LogoutView.as_view(next_page='login'), name="logout"), #In the () make it next_page='dashboard' to redirect 
    #path('', include("encyclopedia.urls")) #Import urls from encyclopedia app 
]
