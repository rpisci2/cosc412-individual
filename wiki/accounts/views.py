from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django import forms
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.conf import settings
from django.contrib import messages
import requests


# Create your views here.



def index(request):
    return render(request, 'index.html')

@login_required(login_url='/warning') #Force users to be redirected to login if they aren't signed in before they can view the wiki
def dashboard(request):
    return render(request, 'dashboard.html')

def register(request):
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():

            #Captcha
            recaptcha_response = request.POST.get('g-recaptcha-response')
            data = {
                'secret' : settings.GOOGLE_RECAPTCHA_SECRET_KEY,
                'response' : recaptcha_response
            }
            r = requests.post('https://www.google.com/recaptcha/api/siteverify',data=data)
            result = r.json()
            if result['success']:
                form.save()
                messages.success(request, 'Captcha Worked!')
                return redirect('login')
                print("Captcha success!")
            else:
                messages.error(request, 'Captcha Failed')
                print("CAPTCHA Failed")
                return redirect('captcha_fail')
        
    else: form = UserCreationForm()
        
    return render(request, 'registration/register.html',{'form':form})

def login_required(request):
    if request.method!="POST":
        return HttpResponse("<h2>Method Not Allowed</h2>")
    else:
        print("Captcha Token : " + request.POST.get("g-recaptcha-response"))

def captcha_fail(request):
    return render(request, 'captcha_fail.html')

